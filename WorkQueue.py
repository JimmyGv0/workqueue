# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import os, sys
import sqlite3
from flask import Flask
from flask import render_template
from flask import request
from flask import url_for
from flask import Response
import json
import time
import random
import thread

def charConv(data, code):
    if data.startswith('\u'):
        return data.decode('unicode_escape').encode('UTF-8')

    if isinstance(data, unicode):
        data = data.encode(code)
    else:
        data = data.decode(code)
    return data

class dbOpts():
    conn = sqlite3.connect('wq.db')

    def read(self, type):
        tSqlBase = "SELECT id,type,source,content,ptime_main,ptime_other,flag from wq_ticket"
        tCond = ""
        try:
            tRet = self.conn.execute(tSqlBase + tCond)
        except:
            return 0
        return tRet

    def write(self, tId, tType, tSource, tContent, tPtimeM, tPtimeO, tFlag):
        tSql = 'INSERT INTO wq_ticket (id,type,source,content,ptime_main,ptime_other,flag) \
            VALUES ("' + tId + '",' + tType + ',"' + tSource + '","' + tContent + '",' + tPtimeM + ',' + tPtimeO + ',' + tFlag + ")"
        try:
            self.conn.execute(tSql)
            self.conn.commit()
            return True
        except:
            return False

    def close(self):
        self.conn.close()

class queueOpts():
    pDB = dbOpts()
    def popAll(self, type):
        return self.pDB.read(type)

    def addNew(self, wqData):
        tType = wqData['ttype']
        tSource = wqData['src']
        tContent = wqData['data']
        tPtimeM = wqData['timetospend']
        tPtimeO = wqData['timetospend_o']
        if self.pDB.write(idGen(), tType, tSource, tContent, tPtimeM, tPtimeO, '1'):
            return True
        else:
            return False

    def setUndo(self):
        pass

def payloadParser(origData):
    pass

def idGen():
    return time.strftime("%Y%m%d%H%M%S", time.localtime()) + str(int(random.uniform(1000, 9999)))

def wqApi():
    typeList = [(0, '默认'), (1, '维护'), (2, '项目'), (3, '紧急'), (4, '邮件'), (5, '其他')]
    statusList = [(0, '已完成'), (1, '新任务'), (2, '进行中'), (3, '等待其他条件以继续'), (4, '放弃')]
    pQueueOpts = queueOpts()
    app = Flask(__name__)
    @app.route('/static/<name>')
    def staticRes():
        return url_for('static', name=name)

    @app.route('/setwq', methods=['GET', 'POST'])
    def setwq():
        pData = request.get_data()
        if not pData:
            return render_template('setwq.html', typeList=typeList)
        try:
            # wqPayload = payloadParser(json.loads(pData))
            ttype = charConv(request.form.get('ttype', '0'), "UTF-8")
            src = charConv(request.form.get('src', '0'), "UTF-8")
            data = charConv(request.form.get('data', 'N/A'), "UTF-8")
            timetospend = charConv(request.form.get('timetospend', '0'), "UTF-8")
            timetospend_o = charConv(request.form.get('timetospend_o', '0'), "UTF-8")
            wqPayload = {'ttype': ttype, 'src': src, 'data': data, 'timetospend': timetospend, 'timetospend_o': timetospend_o}
            if pQueueOpts.addNew(wqPayload):
                return "ok"
            else:
                return "error"
        except:
            return "error"

    @app.route('/getwq')
    def getwq():
        wqList = []
        tRet = pQueueOpts.popAll(0)
        for row in tRet:
            valueList = []
            for column in row:
                valueList.append(column)
            pWq = tuple(valueList)
            wqList.append(pWq)
        return render_template('getwq.html', wqList=wqList, typeList=typeList, statusList=statusList)

    @app.route('/setrule')
    def setrule():
        return 0

    @app.route('/getrule')
    def getrule():
        return 0

    app.run(host='0.0.0.0', port=80)

if os.name == 'nt':
    syscode = "GBK"
else:
    syscode = "UTF-8"

wqApi()